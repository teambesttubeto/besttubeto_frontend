/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { PropTypes } from 'react';
import cx from 'classnames';
import { GoogleMapLoader, GoogleMap, Marker } from 'react-google-maps';
import autoBind from 'react-autobind';
import store from '../../src/store';
import POIManager from '../../src/DataManagers/POIManager';
import $ from 'jquery';
import MapActions from '../../src/actions/MapActions';
import MAP_ICONS from '../../src/data/map_icons';


class MarkerWrapper extends React.Component {

  static propTypes = {
    id: PropTypes.string,
    onClick: PropTypes.func,
    icon: PropTypes.string,
    position: PropTypes.object,
  };

  constructor() {
    super();
    autoBind(this);
  }

  componentWillMount() {
  }

  componentDidMount() {
    //var gmPos = new google.maps.LatLng(-34, 151);
    //this.mapObj.panTo(gmPos);
  }

  renderMarker() {
    return (<Marker
      key={this.props.key}
      position={this.props.position}
      icon={this.props.icon}
      onClick={this.onMarkerClick}
    />);
  }

  onMarkerClick(){
    store.dispatch(MapActions.moveToMarker(this.props.id));
  }

  render() {
    return this.renderMarker();
  }

}

export default MarkerWrapper;
