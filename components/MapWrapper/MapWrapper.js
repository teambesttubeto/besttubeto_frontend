/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { PropTypes } from 'react';
import cx from 'classnames';
import { GoogleMapLoader, GoogleMap, Marker, InfoWindow } from 'react-google-maps';
import autoBind from 'react-autobind';
import store from '../../src/store';
import POIManager from '../../src/DataManagers/POIManager';
import $ from 'jquery';
import MapActions from '../../src/actions/MapActions';
import MAP_ICONS from '../../src/data/map_icons';
import MarkerWrapper from './MarkerWrapper';

class MapWrapper extends React.Component {

  static propTypes = {
    onClick: PropTypes.func,
    onMarkerRightclick: PropTypes.func,
    handleMapLoad: PropTypes.func,
    stationMapRef: PropTypes.object,
  };

  constructor() {
    super();
    autoBind(this);
    this.zoom = 17;
    this.markerRefs = [];
  }

  componentWillMount() {
    this.mapObj = null;
    this.setzoomlevel = false;
    this.setState({
      markers: [],
      selectedMarker: null,
    });

    store.subscribe(() => {
      const state = store.getState();
      this.setState({
        markers: state.MapReducer.markers,
        selectedMarker: state.MapReducer.selectedMarker,
      });

      if (state.MapReducer.markers.length > 0) {

        if (state.MapReducer.selectedMarker !== null && state.MapReducer.selectedMarker !== undefined){

          // Marker selected, zoom so that the station is still in the center
          // but also zoom in/out enough so that the selected marker
          // is still in view
          var markerPosition = this.getMarker(state.MapReducer.selectedMarker).data.position;
          var selectedMarkerLatLng = new google.maps.LatLng(markerPosition.lat, markerPosition.lng);
          var station = this.props.stationMapRef;
          var stationLatLng = new google.maps.LatLng(station.lat, station.lng);
          var distance = this.calculateDistanceBetween(stationLatLng, selectedMarkerLatLng);
          var circle = new google.maps.Circle({radius: distance, center: stationLatLng});

          this.mapObj.fitBounds(circle.getBounds());
        }
        else {
          // No marker selected, zoom so that the station is still in the center
          // but also zoom in/out enough so that all markers are still in
          // view
          var station = this.props.stationMapRef;
          var stationLatLng = new google.maps.LatLng(station.lat, station.lng);

          // We have to get the distance to the furthest marker, so loop through
          // each one and test the distance, if it is larger than the currently
          // stored maximum distance, set that as the current maximum distance.
          var maxDist = -1;
          for (var marker of state.MapReducer.markers) {
            var markerPosition = marker.data.position;
            var markerLatLng = new google.maps.LatLng(markerPosition.lat, markerPosition.lng);
            var distance = this.calculateDistanceBetween(stationLatLng, markerLatLng);
            if (distance > maxDist) {
              maxDist = distance;
            }
          }
          var circle = new google.maps.Circle({radius: maxDist, center: stationLatLng});

          this.mapObj.fitBounds(circle.getBounds());
        }
      }
    });
  }

  processPOIList(poilist){
    var poiArray1 = [];
    var poiArray2 = [];
    var index = 0;
    for(var item of poilist){
      if (index>=poilist.length/2){
        poiArray2.push(item);
      }
      else {
        poiArray1.push(item);
      }
      index++;
    }
    /*
    this.placeMarkers(poiArray1).done((data) => {
      setTimeout(()=>{
        this.placeMarkers(poiArray2).done((data) => {

        });
      }, 1000);

    });
    */

    this.placeMarkersCached(poiArray1.concat(poiArray2));
    var location = new google.maps.LatLng(parseFloat(this.props.stationMapRef.lat), parseFloat(this.props.stationMapRef.lng));
    var latlng = {lat: location.lat(), lng: location.lng()};
    this.addMarkerAt("stationid", latlng, MAP_ICONS.station.src + "|" + MAP_ICONS.station.colour);

  }

  placeMarkersCached(poiArray) {
    var markersDoneCount = 0;
    for(var poi of poiArray) {
      var locationData = poi.mapposition;
      var location = new google.maps.LatLng(locationData.lat, locationData.lng);
      var latlng = { lat: location.lat(), lng: location.lng()};
      this.addMarkerAt(poi.adid, latlng);
    }
  }

  placeMarkers(poiArray){

    var deferred = $.Deferred();
    var markersDoneCount = 0;
    for(var poi of poiArray){
      POIManager.findPlace(poi.admapref, poi.adid)
      .done((data) => {
        var locationData = data.results;
        var code = data.code;
        var location = new google.maps.LatLng(locationData[0].geometry.location.lat(), locationData[0].geometry.location.lng());
        var latlng = { lat: location.lat(), lng: location.lng()};
        this.addMarkerAt(locationData.id, latlng);
      })
      .always((data, code) => {
        markersDoneCount++;
        if (markersDoneCount == poiArray.length){
          deferred.resolve(data);
        }
      });
    }
    return deferred;
  }

  componentDidMount() {
    //var gmPos = new google.maps.LatLng(-34, 151);
    //this.mapObj.panTo(gmPos);
  }

  handleMarkerClick(markerdata) {
    console.log("click marker: " + markerdata.id);

  }

  renderMarkers() {
    if (this.state.markers !== undefined) {
      var markerArray = [];
      var i = 0;
      /*
      for(var markerData of this.state.markers){
        var marker = (
        <Marker
          key={i}
          id={markerData.data.id}
          position={markerData.data.position}
          icon={markerData.data.icon}
          onClick={() => {this.handleMarkerClick(markerData.data)}}
        >
        {this.state.selectedMarker === markerData.data.id && (
          <InfoWindow>
            <div>HEY THERE</div>
          </InfoWindow>
        )}
        </Marker>);
        this.markerRefs[i] = marker;
        markerArray.push(
          marker
        );
        i++;
      }
      */
      this.state.markers.map((marker, index) => {

        var markerElement = (
          <Marker
            key={index}
            position={marker.data.id}
            id={marker.data.id}
            position={marker.data.position}
            icon={marker.data.icon}
            >
          </Marker>
        );
        markerArray.push(markerElement);
      });

      return markerArray;
    }
    return null;
  }

  onMapLoad(data) {
    if (data !== null && data !== undefined) {
      this.mapObj = data;
      if (this.props.handleMapLoad !== undefined){
        this.props.handleMapLoad(this);
      }
    }
    store.dispatch(MapActions.mapLoaded());
    //store.dispatch(MapActions.moveToMarker("stationid"));
  }

  onMarkerClick(markerId){
    store.dispatch(MapActions.moveToMarker(markerId));
  }
  // Geocode can be almost anything 'common sense'
  // i.e. a postcode (SA4 6QH) or a name and location (Mcdonalds, Swansea)
  focusOnLocation(geocode) {
    var deferred = $.Deferred();
    POIManager.findPlace(geocode).done((data)=>{
      var location = new google.maps.LatLng(data[0].geometry.location.lat(), data[0].geometry.location.lng());
      console.log(location);

      deferred.resolve(location);
    }).fail((reason) => {
      deferred.fail(reason);
    });
    return deferred;
  }

  addMarkerAt(id, latlng, icon = MAP_ICONS.default.src + "|" + MAP_ICONS.default.colour){
    store.dispatch(MapActions.addMarker(
      {
        id,
        icon,
        position: latlng,
        defaultAnimation: 2,
        key: Date.now(),
      }
    ));
  }

  getMarker(id){
    for(var m of this.state.markers){
      if (m.data.id === id){
        return m;
      }
    }
    return null;
  }

  calculateDistanceBetween(pos1, pos2) {
    return google.maps.geometry.spherical.computeDistanceBetween(pos1, pos2);
  }

  moveToMarker(id){
    var marker = this.getMarker(id);
    var mapBounds = this.mapObj.getBounds();
    var center = mapBounds.getCenter();
    var latlng = new google.maps.LatLng(marker.data.position.lat, marker.data.position.lng);
    var isInBounds = mapBounds.contains(latlng);
    if (isInBounds===false){
      this.mapObj.panTo(latlng);
    }

  }

  render() {
    const markers = this.renderMarkers();
    var station = this.props.stationMapRef;
    var latlng = new google.maps.LatLng(station.lat, station.lng);
    return (
      <GoogleMapLoader

        containerElement={
          <div
            {...this.props.containerElementProps}
            style={{ height: '100%', }}
          />
        }
        googleMapElement={
          <GoogleMap
            ref={this.onMapLoad}
            defaultZoom={1}
          defaultCenter={{ lat: latlng.lat(), lng: latlng.lng() }}
          >
          {markers}
          </GoogleMap>
        }
      />);
  }

}

export default MapWrapper;
