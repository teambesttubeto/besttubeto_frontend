/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { PropTypes } from 'react';
import Link from '../Link';
import s from './Button.css';
import cx from 'classnames';
import Colours from '../../public/colours.css';
import autoBind from 'react-autobind';

class Button extends React.Component {

  static propTypes = {
    component: PropTypes.oneOf([
      PropTypes.string,
      PropTypes.element,
      PropTypes.func,
    ]),
    type: PropTypes.oneOf(['submit', 'button']),
    to: PropTypes.oneOf([PropTypes.string, PropTypes.object]),
    href: PropTypes.string,
    className: PropTypes.string,
    colored: PropTypes.bool,
    children: PropTypes.node,
    icon: PropTypes.string,
    iconPosition: PropTypes.oneOf(['right', 'left', 'center']),
    tableCell: PropTypes.bool,
    circular: PropTypes.bool,
    iconClass: PropTypes.string,
  };

  componentWillMount() {
    autoBind(this);
  }

  componentDidMount() {
    //window.componentHandler.upgradeElement(this.root);
  }

  componentWillUnmount() {
    //window.componentHandler.downgradeElements(this.root);
  }


  handleClick = (event) => {
    if (this.props.onClick) {
      this.props.onClick(event);
    }

    if (event.button !== 0 /* left click */) {
      return;
    }

    if (event.metaKey || event.altKey || event.ctrlKey || event.shiftKey) {
      return;
    }

    if (event.defaultPrevented === true) {
      return;
    }

    event.preventDefault();

  };

  renderIcon(){
    if (this.props.icon !== undefined) {
      const iconClass = cx(Colours.inngot_text_white, this.props.iconClass === undefined ? '' : this.props.iconClass);
      const className = `fa fa-${this.props.icon}`;

      let style = {
        top: '8px',
        left: '8px',
        position: 'absolute',
      }
      if (this.props.iconPosition === 'center') {
        style = {
          display: 'table',
          margin: 'auto',
        }
      }
      else if (this.props.iconPosition == 'right') {
        style = {
          top: '8px',
          right: '8px',
          position: 'absolute',
        }
      }
      return (<span style={style} className={cx(className, s.icon, iconClass)}/>);
    }
  }

  render() {
    const cssClass =  cx(s.standard_button, (this.props.className === undefined ? '' : this.props.className),
                        (this.props.tableCell == undefined ? '' : s.tableCellButton),
                        (this.props.circular == undefined ? '' : s.circular_button));
    let icon = this.props.icon !== undefined ? this.renderIcon() : undefined;

    if (this.props.type === 'button'){
      return (<button className={cssClass} style={this.props.style} onClick={this.handleClick}>
        <div>
          {icon}
          <span className={Colours.inngot_text_white}>{this.props.children}</span>
        </div>
      </button>);
    }
    else {
      return (<button className={cssClass} style={this.props.style} onClick={this.handleClick}>
        <div>
          {icon}
          <span className={Colours.inngot_text_white}>{this.props.children}</span>
        </div>
      </button>);
    }
  }

}

export default Button;
