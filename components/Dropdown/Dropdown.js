/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { PropTypes } from 'react';
import cx from 'classnames';
import s from './Dropdown.css'
import Link from '../Link';
import autoBind from 'react-autobind';

class Dropdown extends React.Component {

  static propTypes = {
    value: PropTypes.string,
  }

  constructor() {
    super();

  }

  handleChange = (event) => {
    if (this.props.onChange) {
      this.props.onChange(event);
    }

    if (event.button !== 0 /* left click */) {
      return;
    }

    if (event.metaKey || event.altKey || event.ctrlKey || event.shiftKey) {
      return;
    }

    if (event.defaultPrevented === true) {
      return;
    }

    event.preventDefault();

  };

  componentWillMount() {
    autoBind(this);
  }

  componentDidMount() {
    //window.componentHandler.upgradeElement(this.root);
  }

  componentWillUnmount() {
    //window.componentHandler.downgradeElements(this.root);
  }

  render() {
    return (<select className={cx(s.dd_container, this.props.className)} onChange={this.handleChange} value={this.props.value}>
    {this.props.children}
    </select>);
  }

}

export default Dropdown;
