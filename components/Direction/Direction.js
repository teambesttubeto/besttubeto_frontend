/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { PropTypes } from 'react';
import cx from 'classnames';
import s from './Direction.css'
import Link from '../Link';
import autoBind from 'react-autobind';

class Direction extends React.Component {

  static propTypes = {
    data: PropTypes.object,
    mode: PropTypes.string,
  }

  constructor() {
    super();

  }

  handleChange = (event) => {
    if (this.props.onChange) {
      this.props.onChange(event);
    }

    if (event.button !== 0 /* left click */) {
      return;
    }

    if (event.metaKey || event.altKey || event.ctrlKey || event.shiftKey) {
      return;
    }

    if (event.defaultPrevented === true) {
      return;
    }

    event.preventDefault();

  };

  componentWillMount() {

    this.setState({
      directions: null,
      mode: 'overview',
    });
    autoBind(this);
  }

  componentDidMount() {
    //window.componentHandler.upgradeElement(this.root);
  }

  componentWillUnmount() {
    //window.componentHandler.downgradeElements(this.root);
  }

  renderStep(step) {
    const stepLines = this.renderTextLines(step.text);
    return (
      <div className={cx(s.step_container)}>
        <div className={cx(s.step_container_details)}>
          {stepLines}
        </div>
        <div className={cx(s.step_container_time)}>
          {step.time}
        </div>
      </div>
    );
  }

  renderSteps() {
    var stepList = [];
    var i = 0;
    for (var step of this.props.data.detail.steps){
      const step = this.renderStep(step);
      stepList.push(<div key={i} style={{display:'flex', flexDirection:'column', marginLeft: '16px', marginBottom: '4px'}}>
        {step}
      </div>);
      i++;
    }
    return stepList;
  }

  renderTextLines(textlines, textcolour){
    var list = [];
    var i = 0;
    for (var textline of textlines){
      list.push(<span key={i} style={{display:'block', color: textcolour}}>{textline}</span>);
      i++;
    }
    return list;
  }

  renderStepsTime(){
    var list = [];
    for (var step of this.props.data.detail.steps){
      var time = step.time;
      if (time == undefined || time == null){
        time = " ";
      }
      list.push(<div style={{display:'flex', flexDirection:'column', marginLeft: '16px', marginBottom: '16px'}}>
        {time}
      </div>);
    }
    return list;
  }

  renderOverview() {
    const title = this.renderTextLines(this.props.data.overview.title, this.props.data.overview.textColour);
    return (
      <div className={cx(s.direction_container)}>
        <div >
          <div className={cx(s.inner_direction_container)} style={{backgroundColor: this.props.data.overview.backgroundColour}}>
            {title}
          </div>
          <div className={cx()}>{this.props.data.overview.destinationDirection}</div>
        </div>
      </div>
    );
  }

  renderDetail() {
    const steps = this.renderSteps();
    const title = this.renderTextLines(this.props.data.detail.title, this.props.data.overview.textColour);
    return (
      <div className={cx(s.direction_container)}>
        <div className={cx(s.inner_direction_container)} style={{backgroundColor: this.props.data.overview.backgroundColour}}>
          <div>{title}</div>
          <span className={cx()}>{this.props.data.detail.destinationDirection}</span>
        </div>
        <div>
          {steps}
        </div>
      </div>
    );
  }

  render() {
    if (this.props.mode == 'overview'){
      return this.renderOverview();
    }
    else if (this.props.mode == 'detail'){
      return this.renderDetail();
    }
  }

}

export default Direction;
