/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { PropTypes } from 'react';
import cx from 'classnames';
import s from './DirectionContainer.css'
import Link from '../Link';
import autoBind from 'react-autobind';
import Direction from '../Direction';
import Button from '../Button';
import store from '../../src/store';
import MainActions from '../../src/actions/MainActions';
import MapActions from '../../src/actions/MapActions';

class DirectionContainer extends React.Component {

  static propTypes = {
    data: PropTypes.object,
  }

  constructor() {
    super();
    autoBind(this);
  }

  handleChange = (event) => {
    if (this.props.onChange) {
      this.props.onChange(event);
    }

    if (event.button !== 0 /* left click */) {
      return;
    }

    if (event.metaKey || event.altKey || event.ctrlKey || event.shiftKey) {
      return;
    }

    if (event.defaultPrevented === true) {
      return;
    }

    event.preventDefault();

  };

  componentWillMount() {

    this.state = {
      directions: null,
      directionsDisplayMode: 'overview',
      mapOpen: false,
    };

    this.unsubscribe = store.subscribe(() => {
      const state = store.getState();
      this.setState({
        directionsDisplayMode: state.MainReducer.directionsDisplayMode,
        mapOpen: state.MapReducer.mapOpen,
      });
    });
  }

  componentDidMount() {
    //window.componentHandler.upgradeElement(this.root);
  }

  componentWillUnmount() {
    //window.componentHandler.downgradeElements(this.root);
  }

  setMode(_displayMode){
    store.dispatch(MainActions.setDirectionsDisplayMode(_displayMode));
  }

  setModeToDetail(){
    this.resetMapMode();
    this.setMode("detail");
  }

  setModeToOverview(){
    this.resetMapMode();
    this.setMode("overview");
  }

  resetMapMode() {
    if (this.state.mapOpen){
      store.dispatch(MapActions.closeMap());
    }
  }

  renderDirections() {
    var list = [];
    var i = 0;
    for (var direction of this.props.data.items){
      list.push(<Direction key={i} data={direction} mode={this.state.directionsDisplayMode}/>);
      i++;
    }
    return list;
  }

  renderDirectionsViewButton(displayMode){
    if (displayMode == 'overview'){
      return (
        <Button type="button" className={cx(s.view_button, s.option_button,
          this.state.directionsDisplayMode === 'overview' ? s.span_button_fix_overview : s.span_button_fix_detail)}
          onClick={this.setModeToDetail} >
          <span>DETAIL</span>
        </Button>
      );
    }
    else if (displayMode == 'detail')
    {
      return (
        <Button type="button" className={cx(s.view_button, s.option_button,
          this.state.directionsDisplayMode === 'overview' ? s.span_button_fix_overview : s.span_button_fix_detail)}
          onClick={this.setModeToOverview} >
          <span>OVERVIEW</span>
        </Button>
      );
    }
  }

  renderSMSButton() {
    return (
      <Button type="button" className={cx(s.option_button,
        this.state.directionsDisplayMode === 'overview' ? s.span_button_fix_overview : s.span_button_fix_detail)}>
        <span>SMS</span>
      </Button>
    );
  }

  renderPrintButton() {
    return (
      <Button type="button" className={cx(s.option_button,
        this.state.directionsDisplayMode === 'overview' ? s.span_button_fix_overview : s.span_button_fix_detail)}>
        <span>PRINT</span>
      </Button>
    );
  }

  renderMapButton() {
    return (
      <Button type="button" onClick={() => {
        if (this.state.mapOpen) {
          store.dispatch(MapActions.clearMarkers());
          store.dispatch(MapActions.closeMap());
        }else{
          this.setMode("overview");
          store.dispatch(MapActions.openMap());
        }}} className={cx(s.option_button,
       this.state.directionsDisplayMode === 'overview' ? s.span_button_fix_overview : s.span_button_fix_detail)}>
        <span>MAP</span>
      </Button>
    );
  }

  renderDirectionsContainer() {
    const directions = this.state.mapOpen ? null : this.renderDirections();
    if (directions !== null){
      return (<div className={cx(s.white_text)}>
        <div>
          <div className={cx(
            this.state.directionsDisplayMode === 'overview' ? s.inner_directions_summary_container : s.inner_directions_detail_container
            )}
            >
            {directions}
          </div>
          <div className={cx(s.final_step_container)}>
            <span>{this.props.data.detailSummary.finalStep}</span>
            <span className={cx(s.total_travel_time)}>{this.props.data.detailSummary.totalTravelTime}</span>
          </div>
        </div>
      </div>);
    }
    else {
      return null;
    }
  }

  render() {
    if (this.props.data == null || this.props.data == undefined){
      return null;
    }

    const displayModeButton = this.renderDirectionsViewButton(this.state.directionsDisplayMode);
    const smsTextButton = this.renderSMSButton();
    const printButton = this.renderPrintButton();
    const mapButton = this.renderMapButton();
    const directionsContainer = this.renderDirectionsContainer();

    return (
      <div className={cx(this.state.directionsDisplayMode === 'overview' ? s.directions_container : s.directions_container_open,
        this.state.mapOpen ? s.directions_container_map : null)}>
        {directionsContainer}
        <div className={cx(s.button_container, this.state.mapOpen ? s.button_container_map : null)}>
          { displayModeButton }
          { printButton }
          { mapButton }
          { smsTextButton }
        </div>
      </div>);
  }

}

export default DirectionContainer;
