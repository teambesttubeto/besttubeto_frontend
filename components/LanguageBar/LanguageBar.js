/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { PropTypes } from 'react';
import cx from 'classnames';
import s from './LanguageBar.css'
import Link from '../Link';
import autoBind from 'react-autobind';
import LANGUAGES from '../../src/data/languages';
import LanguageFlag from './LanguageFlag';
import localStorage from 'localStorage';
import MainActions from '../../src/actions/MainActions';
import store from '../../src/store';

class LanguageBar extends React.Component {

  static propTypes = {
    data: PropTypes.object,
  }

  constructor() {
    super();

  }

  handleClick = (event) => {
    if (this.props.onChange) {
      this.props.onChange(event);
    }

    if (event.button !== 0 /* left click */) {
      return;
    }

    if (event.metaKey || event.altKey || event.ctrlKey || event.shiftKey) {
      return;
    }

    if (event.defaultPrevented === true) {
      return;
    }

    event.preventDefault();

  };

  componentWillMount() {
    this.state = {
      selectedLanguage: null,
      directions: null,
    };

    autoBind(this);

    store.subscribe(() => {
      const state = store.getState();
      var newSelectedState = state.MainReducer.selectedLanguage === this.props.language;

      this.setState({
        directions: state.MainReducer.directions,
      });
    });
  }

  loadLanguage() {
    var storedLanguage = localStorage.language === undefined ? 'EN' : localStorage.language;
    store.dispatch(MainActions.setLanguage(storedLanguage));
  }

  componentDidMount() {
    //window.componentHandler.upgradeElement(this.root);
    this.loadLanguage();
  }

  componentWillUnmount() {
    //window.componentHandler.downgradeElements(this.root);
  }

  handleOnClick(e) {
    console.log(e);
  }

  renderFlag(flagData) {
    return (
      <LanguageFlag img={flagData.img} language={flagData.code} />
    );
  }

  renderFlags() {
    var flagList = [];
    var i = 0;
    for (var flagData of LANGUAGES.items){
      const flag = this.renderFlag(flagData);
      flagList.push(<div className={s.flagContainer} key={i}>{flag}</div>);
      i++;
    }
    return flagList;
  }

  render() {
    if (this.state.directions === undefined || this.state.directions === null){
      return (<div className={cx(s.languageBarContainer)}>
        <div className={cx(s.languageBar)}>{this.renderFlags()}</div>
      </div>);
    }
    else
    {
      return null;
    }
  }

}

export default LanguageBar;
