/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { PropTypes } from 'react';
import cx from 'classnames';
import s from './LanguageBar.css'
import Link from '../Link';
import autoBind from 'react-autobind';
import store from '../../src/store';
import MainActions from '../../src/actions/MainActions';

class LanguageFlag extends React.Component {

  static propTypes = {
    img: PropTypes.string,
    language: PropTypes.string,
  }

  constructor() {
    super();
  }

  setLanguage(language){
    store.dispatch(MainActions.setLanguage(language));
    if (this.state.directions === undefined) {
      store.dispatch(MainActions.setComeOnDownVisible(true));
    }
  }

  handleClick = (event) => {
    this.setLanguage(this.props.language);

    if (event.button !== 0 /* left click */) {
      return;
    }

    if (event.metaKey || event.altKey || event.ctrlKey || event.shiftKey) {
      return;
    }

    if (event.defaultPrevented === true) {
      return;
    }

    event.preventDefault();

  };

  componentWillMount() {
    autoBind(this);

    this.state = {
      selected: false,
    };

    store.subscribe(() => {
      const state = store.getState();
      var newSelectedState = state.MainReducer.selectedLanguage === this.props.language;
      if (this.refs.flag){
        this.setState({
          selected: newSelectedState,
          directions: state.MainReducer.directions,
        });
      }
    });
  }

  componentDidMount() {
    //window.componentHandler.upgradeElement(this.root);
  }

  componentWillUnmount() {
    //window.componentHandler.downgradeElements(this.root);
  }

  handleOnClick(e) {
    console.log(e);
  }

  render() {
    return (<div className={s.flagInnerContainer} ref={"flag"}>
      <Link to="#" onClick={this.handleClick}>
        <img src={this.props.img} className={cx(s.flagImage, this.state.selected == true ? s.selected : null)}/>
      </Link>
    </div>);
  }

}

export default LanguageFlag;
