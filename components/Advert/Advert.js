/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { PropTypes } from 'react';
import cx from 'classnames';
import { GoogleMapLoader, GoogleMap, Marker } from 'react-google-maps';
import autoBind from 'react-autobind';
import store from '../../src/store';
import POIManager from '../../src/DataManagers/POIManager';
import $ from 'jquery';
import s from './Advert.css';
import MapActions from '../../src/actions/MapActions';


class Advert extends React.Component {

  static propTypes = {
    onClick: PropTypes.func,
    imgSrc: PropTypes.string,
    advertId: PropTypes.string,
    url: PropTypes.string,
  };

  constructor() {
    super();
    autoBind(this);
  }

  componentWillMount() {

    this.state = {
      selected: false,
      mapOpen: false,
    };

    store.subscribe(() => {
      const state = store.getState();
      this.setState({
        selected: state.MapReducer.selectedMarker === this.props.advertId &&
        this.props.advertId !== undefined && this.props.advertId !== null,
        mapOpen: state.MapReducer.mapOpen,
      });
    });
  }

  handleClick(e) {
    e.preventDefault();
    console.log(`Clicked on advertId: ${this.props.advertId}`);
    if (this.state.mapOpen) {
      store.dispatch(MapActions.moveToMarker(this.props.advertId));
    }
    else {
      window.open(this.props.url);
      //window.location.href = this.props.url;
    }
  }

  componentDidMount() {
    //var gmPos = new google.maps.LatLng(-34, 151);
    //this.mapObj.panTo(gmPos);
  }

  render() {
    const cssClasses = cx(this.state.selected === true ? s.tint : null);
    return (
      <div className={cx(s.imageContainer, cssClasses)}>
        <img src={this.props.imgSrc} className={s.image} onClick={this.handleClick}/>
      </div>
    );
  }

}

export default Advert;
