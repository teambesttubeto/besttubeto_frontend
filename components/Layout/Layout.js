/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { PropTypes } from 'react';
import cx from 'classnames';
import Header from './Header';
import Footer from '../Footer';
import s from './Layout.css';
import LanguageBar from '../LanguageBar/LanguageBar';
import store from '../../src/store';

class Layout extends React.Component {

  static propTypes = {
    className: PropTypes.string,
    innerClassName: PropTypes.string,
  };

  componentDidMount() {
  }

  componentWillMount() {

    this.state = {
      languageClass: "language_en",
      backgroundText: "BEST TUBE TO",
    };

    this.unsubscribe = store.subscribe(() => {
      const state = store.getState();
      this.setState({
        languageClass: "language_" + state.MainReducer.selectedLanguage.toLowerCase(),
        backgroundText: state.MainReducer.backgroundText,
      });
    });
  }

  componentWillUnmount() {
  }

  render() {
    const { innerClassName, leftAdverts, rightAdverts, ...normalProps } = this.props;
    return (
      //, s.mainContainer, this.state.languageClass
      <main className={cx(this.props.className)}>
        <div id="pageContainer">
          <div id="leftAdvertsContainer" className={s.advertContainer}>
          {leftAdverts}
          </div>
          <div id="backgroundContainer">
            <span className={cx(s.backgroundTextContainer)}>
              {this.state.backgroundText}
            </span>
            <Header />
            <div {...normalProps} className={cx(s.content, innerClassName)} />
            <LanguageBar />
          </div>
          <div id="rightAdvertsContainer" className={s.advertContainer}>
            {rightAdverts}
          </div>
        </div>
      </main>
    );
  }
}

export default Layout;
