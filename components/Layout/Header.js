/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import Navigation from './Navigation';
import Link from '../Link';
import s from './Header.css';
import autoBind from 'react-autobind';

class Header extends React.Component {

  constructor(){
    super();
    autoBind(this);
  }

  componentDidMount() {
  }

  componentWillUnmount() {
  }

  reloadPage() {
    history.reloadPage();
  }

  render() {
    return (
      <header className={s.header} ref={node => (this.root = node)}>
        <div className={s.headerInnerContainer}>
          <Link className={s.navLink} to="/" onClick={this.reloadPage}>
            <span className="fa fa-home"/>
          </Link>
          <Link raw={true} className={s.navLink} to="mailto:terry@besttubeto.co.uk?subject=Best Tube To contact">
            <span className="fa fa-envelope"/>
          </Link>
        </div>
      </header>
    );
  }

}

export default Header;
