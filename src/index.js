import Sequelize from 'sequelize';
import restify from 'restify';
import Main from './main';
import Station from './models/Station';
import express from 'express';
import path from 'path';
import clc from 'cli-color';


var htmlDir = "/var/www/html";
var advertDir = "/var/www/advertimages/";
var config = {};
var database = {
  database: 'btt',
  username: 'rhodri',
  url: 'localhost:5432',
  password: 'bttadmin2017'
};

var initialiseVariables = () => {
  if (config.html !== undefined) {
    htmlDir = config.html;
  }

  if (config.adverts !== undefined) {
    advertDir = config.adverts;
  }
  if (config.database !== undefined) {
    database = config.database;
  }
}

try {
  if (process.env.BTT_CONFIG === undefined) {
    console.log("Environment variable BTT_CONFIG not defined, using default configuration...");
  }
  else {
    console.log("Loading config from " + process.env.BTT_CONFIG);
    config = require(process.env.BTT_CONFIG);
  }
  initialiseVariables();
} catch(e) {
  if (e.code !== 'MODULE_NOT_FOUND') {
    throw e;
  }
  console.log(clc.cyan("No config file found, assuming running in production..."));
}


console.log("Setting HTML directory to " + htmlDir);
console.log("Setting Advert image directory to " + advertDir);

var app = new express();

app.use((req, res, next) => {
  console.log("request");
  if (/\/adverts\/[^\/]+/.test(req.url)) {
    let pathSplit = req.url.split('/');
    let imageName = pathSplit[pathSplit.length-1];
    console.log(pathSplit);
    console.log("imagename: " + imageName);
    req.url = advertDir + imageName;
    res.sendFile(req.url);
  }
  else {
    next();
  }

});

console.log("Environment: "+ process.env.NODE_ENV);

app.use(express.static(htmlDir));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get(/^((?!\/api\/).)*$/, (req, res, next) => {
  let routePath = path.resolve(htmlDir, htmlDir, 'index.html');
  console.log('Routing: ' + routePath);
  res.sendFile(routePath);
});

app.get('/api/hello/:name', (req, res, next) => {
  res.send('hello' + req.params.name);
})

app.get('/api/stations', (req, res, next) => {
  Main.getStations().then((stations) => {
    var stationsTransformed = {};
    for (var key in stations){
      var station = stations[key];
      stationsTransformed[station.identifier] = station;
    }
    res.json(stationsTransformed);
  });
});

app.get('/api/stations/directions/:from/:to', (req, res, next) => {
  Main.getDirections(req.params.from, req.params.to).then((directions) => {
    Main.getAdverts(req.params.to).then((adverts) => {
      var data = { directions, adverts };
      res.json(data);
    });
  });
});

app.get('/api/adverts/:stationid', (req, res, next) => {
  Main.getAdverts(req.params.stationid).then((adverts) => {
    res.json(adverts);
  });
});
Main.initialise(database.url, database.database, database.username, database.password, __dirname + '/models').then(() => {
  console.log("");
  console.log(clc.green("[ SERVER READY ]"));
  console.log("");
  syncAdverts();
});
var syncAdverts = () => {
  Main.syncToDatabase().then(() =>{
    Main.getAdvertsUnprocessed().then((data) => {
      console.log("Number of unprocessed adverts: " + data.length);
      unprocessedAdvertCount = data.length;
      if (unprocessedAdvertCount > 0){
        Main.processAdverts(data).then(()=>{
          console.log("Finished processing adverts");
        });
      }
      else {
        console.log("No adverts need processing.");
      }
      console.log(models);
    });
  });
}
var unprocessedAdvertCount = 0;
var processingAdverts = false;

var CronJob = require('cron').CronJob;
var job = new CronJob({
  cronTime: '* * * * *',
  onTick: function() {
    console.log("run");
    syncAdverts();
  },
  start: false,
  timeZone: 'Europe/London'
});
job.start();

var server = app.listen(3002);
