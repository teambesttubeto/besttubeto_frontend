export default {
    "items":[
      {"id" : "nandos_eastputney", "geocode": "Nando's SW15 2SW", imgpath : "images/079_eastputney_c_nandos.png"},
      {"id" : "xiong_mao", "geocode" : "Xiong Mao SW15 6TH", imgpath : "images/079_eastputney_a_xiong_mao.png"},
      {"id" : "bills", "geocode" : "Bills SW15", imgpath : "images/079_eastputney_b_bills.png"},
      {"id" : "slater_bradley_co", "geocode" : "Slater Bradley & Co Solicitors London", imgpath : "images/079_eastputney_d_slater_bradley.png"},
      {"id" : "tanning_shop", "geocode" : "Tanning shop SW15 1SL", imgpath : "images/079_eastputney_e_the_tanning_shop.png"},
      {"id" : "magnolia_room", "geocode" : "The magnolia room SW15 1JP", imgpath : "images/079_eastputney_f_the_magnolia_room.png"},
      {"id" : "odeon_cinema", "geocode" : "Odeon cinemas SW15 1SN", imgpath : "images/079_eastputney_g_odeon.png"},
      {"id" : "putney_arts_theatre", "geocode" : "Putney arts theatre SW15 6AW", imgpath : "images/079_eastputney_h_putney_arts_theatre.png"},
      {"id" : "virgin_active", "geocode" : "Virgin Active SW6 7ST", imgpath : "images/079_eastputney_i_virgin_active.png"},
      {"id" : "travellodge", "geocode" : "Travelodge SW6 1NQ", imgpath : "images/079_eastputney_j_travelodge.png"},
      {"id" : "premier_inn", "geocode" : "Premier Inn SW6 3JD", imgpath : "images/079_eastputney_k_premier_inn.png"},
      {"id" : "lodge_hotel", "geocode" : "The lodge hotel SW15 2RN", imgpath : "images/079_eastputney_l_the_lodge_hotel.png"}
    ]
}
