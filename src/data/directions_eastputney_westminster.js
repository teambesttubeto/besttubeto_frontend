export default {
  'id': 'eastputney_westminster',
  'items': [
    {
      'overview': {
        'title': ['Direction 1'],
        'destinationDirection':'Destination',
        'destinationStation':'Destination Station',
        'backgroundColour':'#6a7278',
        'textColour':'#FFFFFF',
        'steps': [
          {
            'exitInstruction':'Exit instruction',
            'destinationInstruction':'Destination instruction',
            'destinationDirection':'Destination direction',
          },
        ],
      },
      'detail': {
        'title': [
          'Direction Line 1',
          'Direction Line 2',
        ],
        'steps': [
          {
            'text': [
              'Walking directions line 1',
              'Walking directions line 2',
            ],
            'time':'Time',
          },
          {
            'text': [
              'Train arrival direction',
              'Boarding direction',
            ],
            'time': '',
          },
          {
            'text': [
              'Possible change direction',
              'Change direction',
            ],
            'time':'Time',
          },
          {
            'text': [
              'Exit direction',
            ],
            'time': '',
          },
        ],
      },
    },
    {
      'overview': {
        'title': ['Direction 2'],
        'destinationDirection':'destination direction',
        'destinationStation':'destination station',
        'backgroundColour': '#007229',
        'textColour' :'#FFFFFF',
        'steps': [
          {
            'exitInstruction':'Exit instruction',
            'destinationInstruction':'Destination instruction',
            'destinationDirection':'Destination direction',
          },
        ],
      },
      'detail': {
        'title': [
          'Direction 2 Line 1',
          'Direction 2 Line 2',
        ],
        'steps': [
          {
            'text': [
              'Walking directions line 1',
              'Walking directions line 2',
            ],
            'time':'Time',
          },
          {
            'text': [
              'Train arrival direction',
              'Boarding direction',
            ],
            'time': '',
          },
          {
            'text': [
              'Exit station line 1',
              'Exit station line 2',
            ],
            'time':'Time',
          },
          {
            'text': [
              'Exit direction line 1',
              'Exit direction line 2',
            ],
            'time':'Time',
          },
        ],
      },
    },
    {
      'overview': {
        'title': ['Direction 3'],
        'destinationDirection':'destination direction',
        'destinationStation':'destination station',
        'backgroundColour': '#0098D8',
        'textColour' :'#FFFFFF',
        'steps': [
          {
            'exitInstruction':'Exit instruction',
            'destinationInstruction':'Destination instruction',
            'destinationDirection':'Destination direction',
          },
        ],
      },
      'detail': {
        'title': [
          'Direction 3 Line 1',
          'Direction 3 Line 2',
        ],
        'steps': [
          {
            'text': [
              'Walking directions line 1',
              'Walking directions line 2',
            ],
            'time':'Time',
          },
          {
            'text': [
              'Train arrival direction',
              'Boarding direction',
            ],
            'time': '',
          },
          {
            'text': [
              'Exit station line 1',
              'Exit station line 2',
            ],
            'time':'Time',
          },
          {
            'text': [
              'Exit direction line 1',
              'Exit direction line 2',
            ],
            'time':'Time',
          },
        ],
      },
    },
    {
      'overview': {
        'title': ['Direction 4'],
        'destinationDirection':'destination direction',
        'destinationStation':'destination station',
        'backgroundColour': '#DC241F',
        'textColour' :'#FFFFFF',
        'steps': [
          {
            'exitInstruction':'Exit instruction',
            'destinationInstruction':'Destination instruction',
            'destinationDirection':'Destination direction',
          },
        ],
      },
      'detail': {
        'title': [
          'Direction 4 Line 1',
          'Direction 4 Line 2',
        ],
        'steps': [
          {
            'text': [
              'Walking directions line 1',
              'Walking directions line 2',
            ],
            'time':'Time',
          },
          {
            'text': [
              'Train arrival direction',
              'Boarding direction',
            ],
            'time': '',
          },
          {
            'text': [
              'Exit station line 1',
              'Exit station line 2',
            ],
            'time':'Time',
          },
          {
            'text': [
              'Exit direction line 1',
              'Exit direction line 2',
            ],
            'time':'Time',
          },
        ],
      },
    },
  ],
  'detailSummary': {
    'finalStep':'Final step (e.g. putney centre is left of station, approx 5 mins walking)',
    'totalTravelTime':'Total travelling time',
  },

  'summarySummary': {
    'travelOverview':'Travel overview (how many steps, stairs, etc)',
    'totalTravelTime':'Total travelling time',
  },
};
