export default {
  "items": [
    {
      "img": "/images/flags/01_flag_union.png",
      "code": "EN",
    },
    {
      "img": "/images/flags/02_flag_spanish.png",
      "code": "ES",
    },
    {
      "img": "/images/flags/03_flag_french.png",
      "code": "FR",
    },
    {
      "img": "/images/flags/04_flag_german.png",
      "code": "DE",
    },
    {
      "img": "/images/flags/05_flag_italian.png",
      "code": "IT",
    },
    {
      "img": "/images/flags/06_flag_japanese.png",
      "code": "JP",
    },
    {
      "img": "/images/flags/07_flag_chinese.png",
      "code": "CN",
    },
    {
      "img": "/images/flags/08_flag_russian.png",
      "code": "RU",
    },
    {
      "img": "/images/flags/09_flag_arab_league.png",
      "code": "AE",
    },
  ]
}
