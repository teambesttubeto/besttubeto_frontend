export default {
    "default":{
      "src": "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2",
      "colour": "FE7569"
    },
    "hover":{
      "src": "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2",
      "colour": "69fe75"
    },
    "station":{
      "src": "http://chart.apis.google.com/chart?chst=d_map_pin_icon&chld=home",
      "colour": "69fe75"
    }
}
