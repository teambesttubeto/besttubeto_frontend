import s from '../stationmain/styles.css';
export default {
  "EN": {
    "come_on_down" : "COME ON DOWN",
    "show_me_text" : "SHOW ME THE BEST TUBE",
    "to" : "TO",
    "from" : "FROM",
    "header_style": s.btt_title_en,
  },
  "ES": {
    "come_on_down" : "BAJA",
    "show_me_text" : "MUÉSTRAME EL MEJOR TUBO PARA",
    "to" : "HACIA",
    "from" : "DE",
    "header_style": s.btt_title_es,
  },
  "FR": {
    "come_on_down" : "VENEZ NOUS VOIR",
    "show_me_text" : "ME MONTRER LE MEILLEUR TUBE",
    "to" : "VERS",
    "from" : "DE",
    "header_style": s.btt_title_fr,
  },
  "DE": {
    "come_on_down" : "KOMM RUNTER",
    "show_me_text" : "ZEIGEN SIE MIR DAS BESTE ROHR ZU",
    "to" : "NACH",
    "from" : "VON",
    "header_style": s.btt_title_de,
  },
  "IT": {
    "come_on_down" : "VIENI GIÙ",
    "show_me_text" : "MOSTRAMI IL TUBO MIGLIORE PER",
    "to" : "VERSO",
    "from" : "DALLA",
    "header_style": s.btt_title_it,
  },
  "JP": {
    "come_on_down" : "に最高のチューブ",
    "show_me_text" : "に私の最高のチューブを表示",
    "to" : "に",
    "from" : "から",
    "header_style": s.btt_title_jp,
  },
  "CN": {
    "come_on_down" : "最好管",
    "show_me_text" : "显示我最好的管",
    "to" : "对",
    "from" : "从",
    "header_style": s.btt_title_cn,
  },
  "RU": {
    "come_on_down" : "ДАВАЙ ВНИЗ",
    "show_me_text" : "ПОКАЗАТЬ МНЕ ЛУЧШУЮ ТРУБКУ К",
    "to" : "К",
    "from" : "из",
    "header_style": s.btt_title_ru,
  },
  "AE": {
    "come_on_down" : "تعال للأسفل",
    "show_me_text" : "تبين لي أفضل أنبوب ل",
    "to" : "نحو",
    "from" : "من عند",
    "header_style": s.btt_title_ae,
  }
}
