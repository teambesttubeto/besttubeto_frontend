/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import localStorage from 'localStorage';
import autoBind from 'react-autobind';
import cx from 'classnames';
import $ from 'jquery';

import Layout from '../../components/Layout';
import s from './styles.css';
import { title, html } from './index.md';
import Dropdown from '../../components/Dropdown/Dropdown'
import Button from '../../components/Button'
import DirectionsManager from '../DataManagers/DirectionsManager';
import DirectionContainer from '../../components/DirectionContainer';
import CommonUtils from '../utils/CommonUtils';
import store from '../store';
import MainActions from '../actions/MainActions';
import LanguageBar from '../../components/LanguageBar'
import LanguageManager from '../DataManagers/LanguageManager';
import StationManager from '../DataManagers/StationManager';
import POIManager from '../DataManagers/POIManager';
import Advert from '../../components/Advert';
import MapWrapper from '../../components/MapWrapper';
import MapActions from '../actions/MapActions';


class StationMainPage extends React.Component {

  constructor() {
    super();
    this.leftAdverts = null;
    this.rightAdverts = null;
    this.mapWrapperObj = null;

    this.leftColumnPositions = [1,2,3,4,5,6];
    this.rightColumnPositions = [7,8,9,10,11,12];

    autoBind(this);
  }

  componentWillMount() {
    this.stations = {};

    this.state = {};

    StationManager.getStations().then((stations) => {
      this.stations = stations;
      for(var station in this.stations){
        this.stations[station].disabled = false;
        this.stations[station].name = this.stations[station].stationname;
      }
      this.state = {
        stationsFrom: Object.assign({},this.stations,{}),
        stationsTo: Object.assign({},this.stations,{}),
        selectedStationFrom: "wembley",
        selectedStationTo: "victoria",
        directions: null,
        changedStation: true,
        language: "EN",
        comeOnDownVisible: true,
        adverts: null,
        mapOpen: false,
      };

      this.loadSelectedStations();

      this.unsubscribe = store.subscribe(() => {
        const state = store.getState();
        this.setState({
          directions: state.MainReducer.directions,
          language: state.MainReducer.selectedLanguage,
          comeOnDownVisible: state.MainReducer.comeOnDownVisible,
          directionsDisplayMode: state.MainReducer.directionsDisplayMode,
          selectedStationTo: state.MainReducer.to,
          selectedStationFrom: state.MainReducer.from,
          adverts: state.MainReducer.adverts,
          mapOpen: state.MapReducer.mapOpen,
        });

        this.syncStations(state.MainReducer.to, state.MainReducer.from);

        localStorage.selectedStationFrom = state.MainReducer.from;
        localStorage.selectedStationTo = state.MainReducer.to;

        if (state.MainReducer.comeOnDownVisible === true && !this.timerRunning) {
          window.setTimeout(()=>{ this.hideComeOnDownText(); }, 3000);
          this.timerRunning = true;
        }
        else if (!state.MainReducer.comeOnDownVisible) {
          this.timerRunning = false;
        }
        if (state.MainReducer.directions !== undefined && state.MainReducer.directions !== null) {
          $('body').addClass('transparent');
        }
        else {
          $('body').removeClass('transparent');
        }
      });

      this.loadSelectedLanguage();

      this.timerRunning = false;
    });
  }

  processAdverts(adverts) {
    this.leftAdverts = adverts.slice(0);
    this.rightAdverts = adverts.slice(0);

    this.leftAdverts = this.leftAdverts.splice(0, Math.floor(adverts.length/2));
    this.rightAdverts = this.rightAdverts.splice(Math.floor(adverts.length/2), adverts.length);
  }

  componentDidMount() {
    document.title = title;
  }

  loadSelectedLanguage() {
    store.dispatch(MainActions.loadSelectedLanguage());
  }

  loadSelectedStations() {
    store.dispatch(MainActions.loadSelectedStations());
  }

  renderAdverts(advertJson, allowedPositions) {
    /*
    var adverts = [];
    var i = 0;
    for(var advert of advertJson){
      adverts.push(<Advert key={i} imgSrc={`images/adverts/${advert.adid}.png`} advertId={advert.id}/>);
      i++;
    }
    return adverts;
    */
    return this.renderAdvertsPositioned(advertJson, allowedPositions);
  }


  renderAdvertsPositioned(advertJson, allowedPositions) {
    let sortedAdverts = CommonUtils.sortAdvertsAscending(advertJson);
    let minAllowedPosition = Math.min(...allowedPositions);
    let maxAllowedPosition = Math.max(...allowedPositions);
    let adverts = new Array();
    let advertPosition = 0;
    let added = false;
    sortedAdverts = sortedAdverts.filter((advert) => { return allowedPositions.indexOf(advert.adposition) > -1;});

    for (let i = minAllowedPosition; i <= maxAllowedPosition; i += 1) {
      let advertData = sortedAdverts[advertPosition];
      added = false;
      if (advertData !== null && advertData !== undefined) {
        if (allowedPositions.indexOf(advertData.adposition) > -1) {
          if (i === advertData.adposition) {
            adverts.push(<Advert key={i} imgSrc={`images/adverts/${advertData.adid}.png`} advertId={advertData.adid} url={advertData.adurl}/>);

            added = true;
            advertPosition++;
          }
        }
      }
      if (!added) {
        adverts.push(<Advert key={i} advertId={null} />);
      }
    }

    return adverts;
  }

  // Sync the stations dropdowns:
  // 1. Enable all station options in both select dropdowns
  // 2. Disable station selected in 'from' residing in 'to'
  // 3. Disable station selected in 'to' residing in 'from'
  syncStations(stationFrom, stationTo) {
    var _newState = Object.assign({}, this.state, {});
    for (var key in _newState.stationsFrom){
      _newState.stationsFrom[key].disabled = false;
    }
    for (var key in _newState.stationsTo){
      _newState.stationsTo[key].disabled = false;
    }
    if (stationTo !== undefined && stationTo !== 'undefined'){
      this.state.stationsFrom[stationTo].disabled = true;
    }
    if (stationFrom !== undefined && stationFrom !== 'undefined'){
      this.state.stationsTo[stationFrom].disabled = true;
    }

    this.setState({
      stationsTo: _newState.stationsTo,
      stationsFrom: _newState.stationsFrom,
    });
  }

  handleSearchButtonClick(){
    this.findDirections();
    this.setState({
      changedStation: false,
    });
  }

  findDirections() {

    store.dispatch(MapActions.clearMarkers());
    store.dispatch(MapActions.unloadMap());

    store.dispatch(
      MainActions.attemptToRetrieveDirections(
        this.state.selectedStationFrom,
        this.state.selectedStationTo,
        this.stations[this.state.selectedStationTo].stationmapref,
      )
    )
    .done(() => {
      var name = this.state.stationsTo[this.state.selectedStationTo].name;
      store.dispatch(MainActions.setBackgroundText(name));
    });
  }

  swapSelectedStations() {
    store.dispatch(MapActions.clearMarkers());
    store.dispatch(MapActions.unloadMap());

    store.dispatch(MainActions.swapDirections());
    store.dispatch(MainActions.attemptToRetrieveDirections(
      this.state.selectedStationTo, this.state.selectedStationFrom));

    var name = this.state.stationsFrom[this.state.selectedStationFrom].name;
    store.dispatch(MainActions.setBackgroundText(name));
  }

  onStationFromDropdownChange(e) {
    var stationId = e.target.value;
    /*
    this.setState({
      selectedStationFrom: stationId,
      changedStation: true,
    });
    this.syncStations(stationId, this.state.selectedStationTo);

    localStorage.selectedStationFrom = stationId;
    */
    store.dispatch(MainActions.changeStation('from', stationId));
    this.setState({
      changedStation: true,
    });
  }

  onStationToDropdownChange(e) {
    var stationId = e.target.value;
    /*
    this.setState({
      selectedStationTo: stationId,
      changedStation: true,
    });
    */
    store.dispatch(MainActions.changeStation('to', stationId));
    this.setState({
      changedStation: true,
    });
    /*this.syncStations(this.state.selectedStationFrom, stationId);*/

    /*localStorage.selectedStationTo = stationId;*/
  }

  hideComeOnDownText(){
    store.dispatch(MainActions.setComeOnDownVisible(false));
  }

  onMapLoaded(data) {
    this.mapWrapperObj = data;
    this.mapWrapperObj.processPOIList(this.state.adverts);
  }

  renderSelectOptionsFromStations(stations, currentSelectedStation){
    // First sort the stations alphabetically
    var stationArray = [];
    var noSelection = currentSelectedStation === undefined;
    for (var key in stations) {
      var stationObject = stations[key];
      stationObject._id = key;
      stationArray.push(stationObject);
    }

    var sortedStations = CommonUtils.sortStationsAlphabetical(stationArray);

    var list = [];
    var i = 0;

    if (noSelection){
      list.push(<option key={6789} value={null} disabled selected>Please select</option>);
    }

    for (var stationObject of sortedStations) {
      list.push(<option key={i} value={stationObject._id} disabled={stationObject.disabled}>{stationObject.name}</option>);
      i++;
    }
    return list;
  }

  renderTitle(languageData) {
    var title_css_class = languageData.header_style === undefined ? s.btt_title : languageData.header_style;
    if (!this.state.mapOpen){
      return (<h1 className={cx(s.btt_title, title_css_class)}>
        {languageData.show_me_text}
      </h1>);
    }
    else {
      return null;
    }
  }

  renderStage1(languageData) {
    return (<div className={s.pulsate}>
      {languageData.come_on_down}
    </div>);
  }

  renderStage2(languageData) {
    var buttonContent = (<div><span className="fa fa-arrow-up" /><span className="fa fa-arrow-down" /></div>);
    var buttonAction = 'swap';


    if (this.state.changedStation) {
      buttonContent = (<span className="fa fa-search" />);
      buttonAction = 'find';
    }

    var content = null;
    var headerText = this.renderTitle(languageData);



    if (!this.state.mapOpen) {
      if (this.state.directionsDisplayMode === 'overview') {
        content = this.state.language === 'AE' ?
        this.renderInputsRTL(languageData, buttonAction, buttonContent) :
        this.renderInputsLTR(languageData, buttonAction, buttonContent);
      }
      else {
        headerText = null;
      }
    }
    else {
      var stationMapRef = this.stations[this.state.selectedStationTo].stationmapref;
      content = (<div className={this.state.mapOpen ? s.mapContainer : null}>
        <MapWrapper handleMapLoad={this.onMapLoaded} stationMapRef={stationMapRef}/>
      </div>);
    }
    return (<div className={cx(s.stationContainer)}>
      <div>
        {headerText}
      </div>
        {content}
      <DirectionContainer data={this.state.directions} />
    </div>);
  }

  renderInputsLTR(languageData, buttonAction, buttonContent) {
    return (
      <div className={cx(s.stationInputContainer)}>
        <div className={cx(s.stationInputsLeftContainer)}>
          <div className={cx(s.stationInputInnerContainer)}>
            <div className={cx(s.stationInputLabel)}><span>{languageData.to}</span></div>
            <Dropdown onChange={this.onStationToDropdownChange} value={this.state.selectedStationTo} className={cx(s.stationInputDropdown)}>
              {this.renderSelectOptionsFromStations(this.state.stationsTo, this.state.selectedStationTo)}
            </Dropdown>
          </div>
          <div className={cx(s.stationInputInnerContainer)} style={{marginTop: '4px'}}>
            <div className={cx(s.stationInputLabel)}><span>{languageData.from}</span></div>
            <Dropdown onChange={this.onStationFromDropdownChange} value={this.state.selectedStationFrom} className={cx(s.stationInputDropdown)}>
              {this.renderSelectOptionsFromStations(this.state.stationsFrom, this.state.selectedStationFrom)}
            </Dropdown>
          </div>
        </div>
        <div className={cx(s.stationInputsRightContainer)}>
          <Button circular={true} type="button" className={s.find_button} onClick={buttonAction === 'find' ? this.handleSearchButtonClick : this.swapSelectedStations} >
            {buttonContent}
          </Button>
        </div>
      </div>
    );
  }

/*
  Not exactly needed yet but anticipating more customization for arabic layout
*/
  renderInputsRTL(languageData, buttonAction, buttonContent) {
    return (
      <div className={cx(s.stationInputContainer, s.rtl)}>
        <div className={cx(s.stationInputsLeftContainer)}>
          <div className={cx(s.stationInputInnerContainer)}>
            <div className={cx(s.stationInputLabel)}><span>{languageData.to}</span></div>
            <Dropdown onChange={this.onStationToDropdownChange} value={this.state.selectedStationTo} className={cx(s.stationInputDropdown)}>
              {this.renderSelectOptionsFromStations(this.state.stationsTo, this.state.selectedStationTo)}
            </Dropdown>
          </div>
          <div className={cx(s.stationInputInnerContainer)} style={{marginTop: '4px'}}>
            <div className={cx(s.stationInputLabel)}><span>{languageData.from}</span></div>
            <Dropdown onChange={this.onStationFromDropdownChange} value={this.state.selectedStationFrom} className={cx(s.stationInputDropdown)}>
              {this.renderSelectOptionsFromStations(this.state.stationsFrom, this.selectedStationFrom)}
            </Dropdown>
          </div>
        </div>
        <div className={cx(s.stationInputsRightContainer)}>
          <Button circular={true} type="button" className={s.find_button} onClick={buttonAction === 'find' ? this.handleSearchButtonClick : this.swapSelectedStations} >
            {buttonContent}
          </Button>
        </div>
      </div>
    );
  }

  render() {
    if (this.state.language === undefined) {
      return null;
    }

    var languageData = LanguageManager.getLanguageData(this.state.language);
    var content = this.state.comeOnDownVisible === true ? this.renderStage1(languageData) : this.renderStage2(languageData);
    const sizeClass = this.state.directionsDisplayMode === 'detail' || this.state.mapOpen ? s.directionsOpen : s.mainContainer;

    var leftAdverts = null;
    var rightAdverts = null;

    if (this.state.adverts !== undefined && this.state.adverts !== null) {
      this.processAdverts(this.state.adverts);
      leftAdverts = this.renderAdverts(this.state.adverts, this.leftColumnPositions);
      rightAdverts = this.renderAdverts(this.state.adverts, this.rightColumnPositions);
    }

    return (
      <Layout className={cx(s.mainContainer)} innerClassName={sizeClass} leftAdverts={leftAdverts} rightAdverts={rightAdverts}>
        <div className={s.contentContainer}>
          {content}
        </div>
      </Layout>
    );
    //  <DirectionContainer data={this.state.directions}/>
  }

}

export default StationMainPage;
