import Q from 'q';
import Main from '../main';

class ColourManager {

  getColour(c) {
    const deferred = Q.defer();

    Main.models['colour'].find({where: {colour: c}}).then((data) => {
      if (data !== null){
        var plainData = data.get({plain: true});
        deferred.resolve(plainData);
      }
      else {
        deferred.resolve(null);
      }
    });

    return deferred.promise;
  }
}

export default new ColourManager();
