import STATION_DATA_1 from '../staticdata/directions_victoria';
import STATION_DATA_2 from '../staticdata/directions_wembleypark';
import STATION_DATA_3 from '../staticdata/directions_westminster';
import Q from 'q';
import Main from '../main';
import ColourManager from './ColourManager';

class DirectionsManager {
  getDirectionsDataTest(stationFrom, stationTo) {
    // For now just return the default data for eastputney to victoria:
    let stationData = STATION_DATA_1;

    if (stationFrom === 'east_putney' && stationTo === 'victoria') {
      stationData = STATION_DATA_2;
    }

    if (stationFrom === 'east_putney' && stationTo === 'westminster') {
      stationData = STATION_DATA_3;
    }

    return stationData;
  }

  getDirectionsData(from, to) {
    const deferred = Q.defer();
    console.log("request directions from " + from + " to " + to + "... ");

    Main.models['direction'].findAll({where: {stationto: to, stationfrom: from}}).then((data) => {
      //console.log(data);
      if (data.length > 0){
        var plainData = data[0].get({plain: true});
        //deferred.resolve(data);
        this.buildDirectionsData(plainData).then((items) => {
          deferred.resolve(items);
        });

        data[0].increment(
          'hits'
        ).then((result) => {
          console.log("hits update result");
          console.log(result);
        });

      }
      else {
        deferred.resolve(null);
      }
    });

    // TEST CODE
    //deferred.resolve(this.getDirectionsDataTest(from, to));
    return deferred.promise;
  }

  buildDirectionsData(data) {
    if (data === undefined || data === null) return {};
    var deferred = Q.defer();

    var rawData = data;
    //var items = this.buildDirections(rawData);
    this.buildDirections(rawData).then((items) => {
      var detailSummary = {finalStep: rawData.notes_from_station, totalTravelTime: rawData.total_travel_time};
      var summarySummary = {travelOverview: rawData.total_travel_details, totalTravelTime: rawData.total_travel_time, journeyOverview: rawData.alternative};
      var mainObj = {general: {
        description: rawData.description,
        note_from_station: rawData.note_from_station,
        hits: rawData.hits
      },directions: items, detailSummary, summarySummary};
      deferred.resolve(mainObj);
    });

    return deferred.promise;
  }

  checkForLineData(data, line_count) {
    var line_data = data[`line_colour_${line_count}`];

    var result =  (line_data !== undefined && line_data !== null);
    return result;
  }

  getLineVisible(line_data) {
    return line_data.line_colour !== null;
  }

  buildDirections(data) {
    var items = [];

    items.push(this.buildDirection(data, '1'));
    items.push(this.buildDirection(data, '2'));
    items.push(this.buildDirection(data, '3'));
    items.push(this.buildDirection(data, '4'));

    this.setSequentialStationLinks(items);
    var lastDirectionIndex = this.setVisibilityOfStations(items);

    if (data.exit_name_5 !== null) {
      // If there's data in the final line then we need to link that up with
      // the last entry in the items array
      var item = items[3];
      item.overview.end_station_link = data.exit_name_5;
      item.overview.line_direction_link = data.note_from_station_5;
      item.overview.change_station_line = data.exit_name_5;
      item.overview.change_station_details_link = data.exit_details_5;
    }

    items = this.remapVisibleDirectionItems(items);

    items = this.remapDirectionTimes(items, data, lastDirectionIndex);

    var deferred = Q.defer();

    this.setColourOfStations(items).then((newItems) => {
      deferred.resolve(newItems);
    });

    return deferred.promise;
  }

  remapDirectionTimes(items, data, lastDirectionIndex) {
    var newItems = [];
    items.map((item, line_count) => {
      item.overview = this.amendForWeirdLogic(item.overview, data, line_count, lastDirectionIndex);
      newItems.push(item);
    });
    return newItems;
  }

  setSequentialStationLinks(items) {
    items.map((item, index) => {
      // Perform this for all items in the array except the last one
      item.overview.change_station_line = null;
      item.overview.line_direction_link = null;
      item.overview.end_station_link = null;
      item.overview.change_station_details_link = null;

      if (index < items.length-1) {
        item.overview.end_station_link = items[index+1].overview.end_station;
        item.overview.line_direction_link = items[index+1].overview.line_direction;
        item.overview.change_station_line = items[index+1].overview.line_name;
        item.overview.change_station_details_link = items[index+1].overview.change_details;
      }
    });
  }

  remapVisibleDirectionItems(items){
    var newItems = [];
    items.map((item, index) => {
      if (item.direction_visible === true) {
        newItems.push(item);
      }
    });
    return newItems;
  }

  setVisibilityOfStations(items){
    var lastDirectionIndex = 0;
    items.map((item, index) => {
      if (this.getLineVisible(item.overview)){
        item.direction_visible = true;
        lastDirectionIndex ++;
      } else {
        item.direction_visible = false;
      }
    });
    return lastDirectionIndex;
  }

  setColourOfStations(items) {
    var deferred = Q.defer();
    var promises = [];
    items.map((item) => {
      promises.push(ColourManager.getColour(item.overview.line_colour));
    });
    Q.all(promises).then((newItems) => {
      newItems.map((item, index) => {
        items[index].overview.line_colour = item.colourcode;
        items[index].overview.line_colour_name = item.colour_name;
      });
      deferred.resolve(items);
    });
    return deferred.promise;
  }

  buildDirection(data, line_count){
    var overview = this.buildDirectionOverview(data, line_count);
    //var detail = this.buildDirectionDetail(data, line_count);
    overview.line_count = parseInt(line_count);
    return {overview};
  }

  buildDirectionOverview(data, line_count) {
    var overview = {};
    overview.line_name = data[`line_name_${line_count}`];
    overview.line_colour = data[`line_colour_${line_count}`];
    overview.line_direction = data[`line_direction_${line_count}`];
    overview.end_station = data[`end_station_${line_count}`];
    overview.line_platform = data[`line_platform_${line_count}`];
    overview.entrance_details = data[`entrance_details_${line_count}`];
    overview.entrance_time = data[`entrance_time_${line_count}`];
    overview.stand_position = data[`stand_position_${line_count}`];
    overview.arrives_from = data[`arrives_from_${line_count}`];
    overview.board_position = data[`board_position_${line_count}`];
    overview.change_station = data[`change_station_${line_count}`];
    overview.change_details = data[`change_details_${line_count}`];
    overview.station_count = data[`station_count_${line_count}`];
    overview.station_count_text = data[`station_count_text_${line_count}`];
    overview.journey_time = data[`journey_time_${line_count}`];
    overview.journey_exit = data[`journey_exit_${line_count}`];



    return overview;
  }

  amendForWeirdLogic(overview, data, line_count, lastDirectionIndex) {

    if (parseInt(line_count) > 0) {
      overview.entrance_time = data[`change_time_${(parseInt(line_count)+1).toString()}`];
    }

    if (parseInt(line_count+1) === lastDirectionIndex) {
      // This is the last line
      if (lastDirectionIndex !== 4) {
        var nextItemIndex = (parseInt(lastDirectionIndex) + 1).toString();

        overview.exit_time = data[`change_time_${nextItemIndex}`];
      }
      else {
        overview.exit_time = data[`exit_time_5`];
      }
    }
    return overview;
  }

  getDirectionsFromTo(from, to) {
    return this.getDirectionsData(from, to);
  }
}

export default new DirectionsManager();
