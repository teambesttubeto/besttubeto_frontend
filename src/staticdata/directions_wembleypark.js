export default {
  'id': 'eastputney_wembleypark',
  'items': [
    {
      'overview': {
        'title': ['Jubilee Line Southbound (Stratford) Platform 4'],
        'destinationDirection':'To Westminster',
        'destinationStation':'12th station',
        'backgroundColour':'#6a7278',
        'textColour':'#FFFFFF',
        'steps': [
          {
            'exitInstruction':'Exit FRONT LEFT',
            'destinationInstruction':'To District Line',
            'destinationDirection':'Westbound (Wimbledon)',
          },
        ],
      },
      'detail': {
        'title': [
          'Jubilee Line (Grey) Southbound',
          'Towards Stratford (Platform 4)',
        ],
        'steps': [
          {
            'text': [
              '60m walk + 25 stairs',
              'to the RIGHT of the platform',
            ],
            'time':'1 minute',
          },
          {
            'text': [
              'Train arrives from the LEFT',
              'Board Train at the FRONT (RIGHT)',
            ],
            'time': '',
          },
          {
            'text': [
              'Change Train at the 12th Station',
              '(Westminster)',
            ],
            'time':'25 minutes',
          },
          {
            'text': [
              'Exit FRONT LEFT to',
            ],
            'time': '',
          },
        ],
      },
    },
    {
      'overview': {
        'title': ['District Line Westbound (Wimbledon Branch) (Platform 1)'],
        'destinationDirection':'To East Putney',
        'destinationStation':'11th station',
        'backgroundColour': '#007229',
        'textColour' :'#FFFFFF',
        'steps': [
          {
            'exitInstruction':'Exit REAR LEFT',
            'destinationInstruction':'To Street',
            'destinationDirection':'Turn LEFT to Putney',
          },
        ],
      },
      'detail': {
        'title': [
          'District Line (Green) Westbound (Wimbledon Branch)',
          'Towards Wimbledon (Platform 1)',
        ],
        'steps': [
          {
            'text': [
              '65m walk + 30 stairs + 1 escalator',
              'to the RIGHT of the platform',
            ],
            'time':'3 minutes',
          },
          {
            'text': [
              'Train arrives from the RIGHT',
              'Board Train at the REAR (RIGHT)',
            ],
            'time': '',
          },
          {
            'text': [
              'Exit Train at the 11th Station',
              '(East Putney)',
            ],
            'time':'26 minutes',
          },
          {
            'text': [
              'Exit REAR LEFT to street',
              '30m walk + 25 stairs',
            ],
            'time':'1 minute',
          },
        ],
      },
    },
  ],
  'detailSummary': {
    'finalStep':'Putney Centre is LEFT out of the Tube Station - 5 minutes walk. ',
    'totalTravelTime':'Total travelling time 55-60 minutes.',
  },

  'summarySummary': {
    'travelOverview':'155m walk, 80 stairs, 1 escalator.',
    'totalTravelTime':'Total travelling time 55-60 minutes',
  },
};
