class CommonUtils {
  sortStationsAlphabetical(inputArray) {
    var newArray = inputArray.slice(0);
    newArray.sort((a, b) => {
      if (a.name < b.name){
        return -1;
      }
      if (a.name > b.name){
        return 1;
      }
      return 0;
    });
    return newArray;
  }

  sortAdvertsAscending(inputArray) {
    var newArray = inputArray.slice(0);
    newArray.sort((a, b) => {
      if (a.adposition < b.adposition){
        return -1;
      }
      if (a.adposition > b.adposition){
        return 1;
      }
      return 0;
    });
    return newArray;
  }
}

export default new CommonUtils();
