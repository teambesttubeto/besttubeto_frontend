import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import MapReducer from './reducers/MapReducer';
import MainReducer from './reducers/MainReducer';
//import LoginReducer from './Reducers/LoginReducer';
const loggerMiddleware = createLogger();

// Centralized application state
// For more information visit http://redux.js.org/

// We need to supply a single reducer object to the store, so we use combineReducers to do this.
const combinedReducers = combineReducers(
  {
    MapReducer,
    MainReducer,
  }
);

// Create the store which holds the current state of the application
const store = createStore(combinedReducers, applyMiddleware(thunkMiddleware, loggerMiddleware));

export default store;
