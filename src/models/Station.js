import Sequelize from 'sequelize';

export default {

  model: {
    stationid: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    stationname: {
      type: Sequelize.STRING,
    },
    identifier: {
      type: Sequelize.STRING,
    },
    language: {
      type: Sequelize.STRING,
    },
    stationmapref: {
      type: Sequelize.STRING,
    }
  },

  relations: {},

  options: {
    schema: 'data',
    tableName: 'stations',
    timestamps: false,
  }
};
