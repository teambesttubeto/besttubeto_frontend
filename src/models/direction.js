import Sequelize from 'sequelize';

export default {

  model: {
    stationfrom: {
      type: Sequelize.STRING,
      unique: 'directions_compound_pkey',
      primaryKey: true,
    },
    stationto: {
      type: Sequelize.STRING,
      unique: 'directions_compound_pkey',
    },
    description: {
      type: Sequelize.STRING,
    },
    line_name_1: {
      type: Sequelize.STRING,
    },
    line_name_2: {
      type: Sequelize.STRING,
    },
    line_name_3: {
      type: Sequelize.STRING,
    },
    line_name_4: {
      type: Sequelize.STRING,
    },
    line_colour_1: {
      type: Sequelize.STRING,
    },
    line_colour_2: {
      type: Sequelize.STRING,
    },
    line_colour_3: {
      type: Sequelize.STRING,
    },
    line_colour_4: {
      type: Sequelize.STRING,
    },
    line_direction_1: {
      type: Sequelize.STRING,
    },
    line_direction_2: {
      type: Sequelize.STRING,
    },
    line_direction_3: {
      type: Sequelize.STRING,
    },
    line_direction_4: {
      type: Sequelize.STRING,
    },
    end_station_1: {
      type: Sequelize.STRING,
    },
    end_station_2: {
      type: Sequelize.STRING,
    },
    end_station_3: {
      type: Sequelize.STRING,
    },
    end_station_4: {
      type: Sequelize.STRING,
    },
    line_platform_1: {
      type: Sequelize.STRING,
    },
    line_platform_2: {
      type: Sequelize.STRING,
    },
    line_platform_3: {
      type: Sequelize.STRING,
    },
    line_platform_4: {
      type: Sequelize.STRING,
    },
    entrance_details_1: {
      type: Sequelize.STRING,
    },
    entrance_time_1: {
      type: Sequelize.STRING,
    },
    stand_position_1: {
      type: Sequelize.STRING,
    },
    stand_position_2: {
      type: Sequelize.STRING,
    },
    stand_position_3: {
      type: Sequelize.STRING,
    },
    stand_position_4: {
      type: Sequelize.STRING,
    },
    arrives_from_1: {
      type: Sequelize.STRING,
    },
    arrives_from_2: {
      type: Sequelize.STRING,
    },
    arrives_from_3: {
      type: Sequelize.STRING,
    },
    arrives_from_4: {
      type: Sequelize.STRING,
    },
    board_position_1: {
      type: Sequelize.STRING,
    },
    board_position_2: {
      type: Sequelize.STRING,
    },
    board_position_3: {
      type: Sequelize.STRING,
    },
    board_position_4: {
      type: Sequelize.STRING,
    },
    change_station_1: {
      type: Sequelize.STRING,
    },
    change_station_2: {
      type: Sequelize.STRING,
    },
    change_station_3: {
      type: Sequelize.STRING,
    },
    change_station_4: {
      type: Sequelize.STRING,
    },
    change_details_2: {
      type: Sequelize.STRING,
    },
    change_details_3: {
      type: Sequelize.STRING,
    },
    change_details_4: {
      type: Sequelize.STRING,
    },
    station_count_1: {
      type: Sequelize.STRING,
    },
    station_count_2: {
      type: Sequelize.STRING,
    },
    station_count_3: {
      type: Sequelize.STRING,
    },
    station_count_4: {
      type: Sequelize.STRING,
    },
    station_count_text_1: {
      type: Sequelize.STRING,
    },
    station_count_text_2: {
      type: Sequelize.STRING,
    },
    station_count_text_3: {
      type: Sequelize.STRING,
    },
    station_count_text_4: {
      type: Sequelize.STRING,
    },
    change_time_2: {
      type: Sequelize.STRING,
    },
    change_time_3: {
      type: Sequelize.STRING,
    },
    change_time_4: {
      type: Sequelize.STRING,
    },
    journey_time_1: {
      type: Sequelize.STRING,
    },
    journey_time_2: {
      type: Sequelize.STRING,
    },
    journey_time_3: {
      type: Sequelize.STRING,
    },
    journey_exit_1: {
      type: Sequelize.STRING,
    },
    journey_exit_2: {
      type: Sequelize.STRING,
    },
    journey_exit_3: {
      type: Sequelize.STRING,
    },
    journey_time_4: {
      type: Sequelize.STRING,
    },
    journey_exit_4: {
      type: Sequelize.STRING,
    },
    exit_name_5: {
      type: Sequelize.STRING,
    },
    note_from_station_5: {
      type: Sequelize.STRING,
    },
    exit_details_5: {
      type: Sequelize.STRING,
    },
    exit_time_5: {
      type: Sequelize.STRING,
    },
    notes_from_station: {
      type: Sequelize.STRING,
    },
    total_travel_details: {
      type: Sequelize.STRING,
    },
    total_travel_time: {
      type: Sequelize.STRING,
    },
    alternative: {
      type: Sequelize.STRING,
    },

    language: {
      type: Sequelize.STRING,
    },

    hits: {
      type: Sequelize.BIGINT,
    },
  },

  relations: {},

  options: {
    schema: 'data',
    tableName: 'directions_new',
    timestamps: false,
  }
};
