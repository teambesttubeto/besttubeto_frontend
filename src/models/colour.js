import Sequelize from 'sequelize';

export default {

  model: {
    colour: {
      type: Sequelize.STRING,
      primaryKey: true,
    },
    colourcode: {
      type: Sequelize.STRING,
    },
    colour_name: {
      type: Sequelize.STRING,
    }
  },

  relations: {},

  options: {
    schema: 'data',
    tableName: 'colours',
    timestamps: false,
  }
};
