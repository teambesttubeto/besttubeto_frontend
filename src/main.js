<<<<<<< HEAD
import Sequelize from 'sequelize';
import restify from 'restify';
import filesystem from 'fs';
import Q from 'q';
import path from 'path';
import express from 'express';
import DirectionsManager from './managers/DirectionsManager';
import advertpositionmappings from './staticdata/advertpositionmappings';
import GoogleMapsAPI from 'googlemaps';
import clc from 'cli-color';

var models = null;

class Main {

  constructor() {
    this.sequelize = null;
    this.server = null;
    this.fileRouter = null;
    this.apiRouter = null;

    this.models = {};
    this.relationships = {};
    this.googleMapsAPI = null;

    this.publicConfig = {
      key: 'AIzaSyDMPxkrsToC-nxQVLczIGPHEnat2nRsf3M',
      stagger_time:       1000, // for elevationPath
      encode_polylines:   false,
      secure:             false, // use https
    };
  }

  initialise(dburl, database, username, password, modelsPath) {
    const deferred = Q.defer();

    this.initialiseSequelize(dburl, database, username, password).then(() => {
      console.log(clc.green("Database initialised"));
      console.log("Initialising models...");
      this.loadModels(modelsPath).then(() =>{
        console.log(clc.green("initialised models"));
        console.log("initialising google maps API...");
        this.googleMapsAPI = new GoogleMapsAPI(this.publicConfig);
        console.log(clc.green("initialised google maps API"));
        deferred.resolve();
      });
    }, (err) => {
      console.log(clc.red('Could not connect to database:'));
      console.log(clc.red(err));
    });

    return deferred.promise;
  }

  initialiseSequelize(dburl, database, username, password){
    console.log("Connecting to database: " + dburl + "/" + database);
    this.sequelize = new Sequelize(database, username, password, {
      host: dburl,
      port: '5432',
      dialect: 'postgres',

      pool: {
        max: 5,
        min: 0,
        idle: 10000
      },
    });
    const deferred = Q.defer();

    this.sequelize.authenticate().then((err) => {
      console.log(clc.green("Connection established"));
      deferred.resolve(err);
    })
    .catch((err) => {
      console.log(clc.red("Connection refused: ", err));
      deferred.reject(err);
    });

    return deferred.promise;
  }

  respond(req, res, next) {
    res.send('hello' + req.params.name);
    next();
  }

  getDirections(from, to) {
    return DirectionsManager.getDirectionsData(from, to);
  }

  getStations() {
    console.log("request stations...");
    var deferred = Q.defer();
    this.models['station'].findAll().then((data) => {

      var transformedData = [];
      for (var key in data) {
        var station = data[key].get({plain: true});
        var lng = null;
        var lat = null;
        if (station.stationmapref !== null){
          var textParts = station.stationmapref.split(',');
          lng = textParts[1];
          lat = textParts[0];
        }

        station.stationmapref = { lng, lat };
        transformedData.push(station);
      }

      deferred.resolve(transformedData);
    });

    return deferred.promise;
  }

  getAdvertsUnprocessed(){
    var deferred = Q.defer();

    this.models['advert'].findAll({
      where: {
        $or: [
        {adlatlong: null },
        {placeid: null }
      ]
    }
    }).then((data) => {
      deferred.resolve(data);
    });
    return deferred.promise;
  }

  getAdverts(stationid) {
    var deferred = Q.defer();

    this.models['advert'].findAll({
      where: {
        stationid: stationid,
      }
    }).then((data) => {
      // transform the advert positions into the mappings for ease of use on
      // the frontend.
      //console.log(data);
      var transformedData = new Array();

      for (var key in data) {
        var advert = data[key].get({plain: true});
        //console.log(advert);
        var newAdvertPosition = advertpositionmappings[advert.adposition.toString()];
        //console.log(newAdvertPosition);
        advert.adposition = parseInt(newAdvertPosition);

        var lng = null;
        var lat = null;
        if (advert.adlatlong !== null){
          var textParts = advert.adlatlong.split(',');
          lng = textParts[1];
          lat = textParts[0];
        }

        advert.mapposition = { lng, lat };
        transformedData.push(advert);
      }
      deferred.resolve(transformedData);
    });
    return deferred.promise;
  }

  syncToDatabase() {
    var deferred = Q.defer();
    this.sequelize.sync().then(() => {
      deferred.resolve();
    });
    return deferred.promise;
  }

  processAdverts(data) {
    var funcs = [];
    var deferred = Q.defer();
    console.log("Processing adverts...");
    var chain = data.reduce((previous, item) => {
      return previous.then((previousValue) => {
        return this.processAdvert(item);
      });
    }, Q.resolve()).then(() => {
      deferred.resolve();
    });

    return deferred.promise;
  }

  processAdvert(advertData) {
    var deferred = Q.defer();
    var advertPlain = advertData.get({plain:true});

    this.findMapRef({address:advertPlain.admapref}).then((result) => {
      console.log(clc.green("Successfully found location, updating database record..."));
      console.log(result.geometry.location);
      var latLong = result.geometry.location.lat.toString() + ',' + result.geometry.location.lng.toString();
      var placeid = result.place_id;
      advertData.update({
        adlatlong: latLong,
        placeid: placeid,
      }).then((result) => {
        console.log(result);
        console.log(clc.green("successfully updated record, syncing ORM to database..."));

        advertData.reload().then(() => {
          console.log(clc.green("Synced record to database."));
          deferred.resolve();
        });
      });
    },(err) => {
      console.log(clc.red("Error processing adverts from database: "));
      console.log(clc.red("No admapref for advert '" + advertPlain.adid + "'"));
      deferred.reject(err);
    });

    return deferred;
  }

  loadModels(modelsPath) {
    const deferred = Q.defer();

    this.models = {};
    this.relationships = {};

    filesystem.readdirSync(modelsPath).forEach((name) => {
       var object = require(modelsPath + "/" + name).default;
       var options = object.options || {};
       var modelName = name.replace(/\.js$/i, "");

       this.models[modelName] = this.sequelize.define(modelName, object.model, options);
       if("relations" in object){
           this.relationships[modelName] = object.relations;
       }
       models = this.models;
       console.log(this.models);
     });
     deferred.resolve();
     return deferred.promise;
  }

  findMapRef(geocode) {
    var deferred = Q.defer();
    if (geocode.address === null || geocode.address === undefined) {
      deferred.reject("GMAPI Unable to find location for advert - no admapref on database row.")
    }
    else {
      console.log("GMAPI finding location for " + geocode.address + " ...");
      this.googleMapsAPI.geocode(geocode, ((err, result) => {
        if (err !== undefined && err !== null){
          console.log(err);
          deferred.reject(err);
        }
        else {
          console.log(result);
          deferred.resolve(result.results[0]);
        }
      }));
    }
    return deferred.promise;
  }
}

export { models };
export default new Main;
=======
/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import 'babel-polyfill';
import 'whatwg-fetch';

import React from 'react';
import ReactDOM from 'react-dom';
import FastClick from 'fastclick';
import { Provider } from 'react-redux';

import store from './store';
import router from './router';
import history from './history';

let routes = require('./routes.json').default; // Loaded with utils/routes-loader.js

const container = document.getElementById('container');

function renderComponent(component) {
  ReactDOM.render(<Provider store={store}>{component}</Provider>, container);
}

// Find and render a web page matching the current URL path,
// if such page is not found then render an error page (see routes.json, core/router.js)
function render(location) {
  router.resolve(routes, location)
    .then(renderComponent)
    .catch(error => router.resolve(routes, { ...location, error }).then(renderComponent));
}

// Handle client-side navigation by using HTML5 History API
// For more information visit https://github.com/ReactJSTraining/history/tree/master/docs#readme
history.listen(render);
render(history.location);

// Eliminates the 300ms delay between a physical tap
// and the firing of a click event on mobile browsers
// https://github.com/ftlabs/fastclick
FastClick.attach(document.body);

// Enable Hot Module Replacement (HMR)
if (module.hot) {
  module.hot.accept('./routes.json', () => {
    routes = require('./routes.json').default; // eslint-disable-line global-require
    render(history.location);
  });
}
