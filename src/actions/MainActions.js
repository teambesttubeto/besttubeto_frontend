import DirectionsManager from '../DataManagers/DirectionsManager';

class MainActions {

  LOAD_SELECTED_STATIONS = 'LOAD_SELECTED_STATIONS';

  loadSelectedStations() {
    return {
      type: this.LOAD_SELECTED_STATIONS,
    };
  }

  LOAD_SELECTED_LANGUAGE = 'LOAD_SELECTED_LANGUAGE';

  loadSelectedLanguage() {
    return {
      type: this.LOAD_SELECTED_LANGUAGE,
    };
  }

  SET_LANGUAGE = 'SET_LANGUAGE';

  setLanguage(languageId) {
    return {
      type: this.SET_LANGUAGE,
      languageId,
    };
  }

  CHANGE_STATION = 'CHANGE_STATION';

  changeStation(stationType, newStationId) {
    return {
      type: this.CHANGE_STATION,
      stationType,
      newStationId,
    };
  }

  FIND_DIRECTIONS_REQUEST = 'FIND_DIRECTIONS_REQUEST';

  findDirections(from, to) {
    return {
      type: this.FIND_DIRECTIONS_REQUEST,
      from,
      to,
    };
  }

  FIND_DIRECTIONS_SUCCESS = 'FIND_DIRECTIONS_SUCCESS';

  findDirectionsSuccess(from, to, directions, adverts, stationmapref) {
    return {
      type: this.FIND_DIRECTIONS_SUCCESS,
      from,
      to,
      directions,
      adverts,
      stationmapref,
    };
  }

  FIND_DIRECTIONS_FAILED = 'FIND_DIRECTIONS_FAILED';

  findDirectionsFailed(from, to, error) {
    return {
      type: this.FIND_DIRECTIONS_FAILED,
      from,
      to,
      error,
    };
  }

  SWAP_DIRECTIONS = 'SWAP_DIRECTIONS';

  swapDirections() {
    return {
      type: this.SWAP_DIRECTIONS,
    };
  }

  SET_DIRECTIONS_DISPLAY_MODE = 'SET_DIRECTIONS_DISPLAY_MODE';

  setDirectionsDisplayMode(mode) {
    return {
      type: this.SET_DIRECTIONS_DISPLAY_MODE,
      mode,
    };
  }

  SET_BACKGROUND_TEXT = 'SET_BACKGROUND_TEXT';

  setBackgroundText(backgroundText) {
    return {
      type: this.SET_BACKGROUND_TEXT,
      backgroundText,
    };
  }

  attemptToRetrieveDirections(from, to, stationmapref) {
    return (dispatch) => {
      dispatch(this.findDirections(from, to));

      return DirectionsManager.getDirectionsFromTo(from, to).done((data) => {
        dispatch(this.findDirectionsSuccess(from, to, data.directions, data.adverts, stationmapref));
      }).fail((data) => {
        console.log("Error getting Directions");
        dispatch(this.findDirectionsFailed(from, to, data));
      });
    };
  }

  SET_COME_ON_DOWN_VISIBLE = 'SET_COME_ON_DOWN_VISIBLE';

  setComeOnDownVisible(visible) {
    return {
      type: this.SET_COME_ON_DOWN_VISIBLE,
      visible,
    };
  }
}

export default new MainActions();
