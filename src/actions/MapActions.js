import POIManager from '../DataManagers/POIManager';

class MapActions {

  ADD_MARKER = 'ADD_MARKER';

  addMarker(data) {
    return {
      type: this.ADD_MARKER,
      data,
    };
  }

  REMOVE_MARKER = 'REMOVE_MARKER';

  removeMarker(index) {
    return {
      type: this.REMOVE_MARKER,
      index,
    };
  }

  CLEAR_MARKERS = 'CLEAR_MARKERS';

  clearMarkers() {
    return {
      type: this.CLEAR_MARKERS,
    };
  }

  FOCUS_LOCATION = 'FOCUS_LOCATION';

  focusOnLocation(geocode) {
    return (dispatch) => {
        return POIManager.findPlace(geocode).done((data) => {
          return data;
        }).fail((data) => {
          return null;
      });
    }
  }

  MOVE_TO_MARKER = 'MOVE_TO_MARKER';

  moveToMarker(markerId) {
    return {
      type: this.MOVE_TO_MARKER,
      markerId,
    };
  }

  OPEN_MAP = 'OPEN_MAP';

  openMap() {
    return {
      type: this.OPEN_MAP,
    };
  }

  CLOSE_MAP = 'CLOSE_MAP';

  closeMap() {
    return {
      type: this.CLOSE_MAP,
    };
  }

  MAP_LOADED = 'MAP_LOADED';

  mapLoaded() {
    return {
      type: this.MAP_LOADED,
      mapLoaded: true,
    };
  }

  UNLOAD_MAP = 'UNLOAD_MAP';

  unloadMap() {
    return {
      type: this.UNLOAD_MAP,
      mapLoaded: false,
    };
  }
}

export default new MapActions;
