import LANGUAGE_DATA from '../Data/languagedata';
import $ from 'jquery';

class LanguageManager {
  getLanguageData(language) {
    return LANGUAGE_DATA[language];
  }
}

export default new LanguageManager();
