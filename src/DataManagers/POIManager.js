import POI_DATA from '../Data/poi_eastputney';
import $ from 'jquery';
import HTTPManager from './HTTPManager';

class POIManager {
  getPOIForEndpoint(stationid){
    const deferred = $.Deferred();
    HTTPManager.requestData({
      url: `/api/adverts/${stationid}`
    }).then((adverts) => {
      deferred.resolve(adverts);
    }).fail((err) => {
      deferred.reject(err);
    });
    return deferred;
  }

  findPlace(geocode, id){
    var geocoder = new google.maps.Geocoder();
    const deferred = $.Deferred();
    const promise = geocoder.geocode({'address': geocode}, (results, status) => {
      if (status === 'OK') {
        results.id = id;
        deferred.resolve({ results, code: 200 });
      } else if (status === 'OVER_QUERY_LIMIT') {
        deferred.reject({ reason: 'Query limit reached.', code: 401 });
      } else {
        deferred.reject({ reason: `Unable to find a place with the supplied geocode ${geocode}.`, code: 400 });
      }
    });
    return deferred;
  }
}

export default new POIManager;
