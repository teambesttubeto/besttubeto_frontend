import $ from 'jquery';

class HTTPManager {
  requestData(data) {
    //var dev = process.env.NODE_ENV === 'production' ? false : true;
    var dev = true;
    if (dev) {
      data.url = "http://localhost:3002" + data.url;
    }
    if (data.methodType === undefined){
      data.methodType = 'GET';
    }
    return $.ajax({
      url: data.url,
      data: data.payload,
      contentType: data.contentType,
      type: data.methodType,
    });
  }
}

export default new HTTPManager();
