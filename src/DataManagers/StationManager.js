import $ from 'jquery';
import HTTPManager from './HTTPManager';

class StationManager {
  getStations() {
    const deferred = $.Deferred();
    HTTPManager.requestData({
      url: "/api/stations"
    }).then((stations) => {
      deferred.resolve(stations);
    }).fail((err) => {
      deferred.reject(err);
    });
    return deferred;
  }
}

export default new StationManager();
