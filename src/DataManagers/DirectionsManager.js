import STATION_DATA_1 from '../Data/directions_eastputney_victoria';
import STATION_DATA_2 from '../Data/directions_eastputney_wembleypark';
import STATION_DATA_3 from '../Data/directions_eastputney_westminster';
import HTTPManager from './HTTPManager';
import $ from 'jquery';

class DirectionsManager {
  getDirectionsDataTest(stationFrom, stationTo) {
    // For now just return the default data for eastputney to victoria:
    let stationData = STATION_DATA_1;

    if (stationFrom === 'east_putney' && stationTo === 'victoria') {
      stationData = STATION_DATA_2;
    }

    if (stationFrom === 'east_putney' && stationTo === 'westminster') {
      stationData = STATION_DATA_3;
    }

    return stationData;
  }

  getDirectionsData(from, to) {
    const deferred = $.Deferred();
    HTTPManager.requestData({
      url: `/api/stations/directions/${from}/${to}`,
    }).then((stations) => {
      deferred.resolve(stations);
    }).fail((err) => {
      deferred.reject(err);
    });
    return deferred;
  }

  getDirectionsFromTo(from, to) {
    return this.getDirectionsData(from, to);
  }
}

export default new DirectionsManager();
