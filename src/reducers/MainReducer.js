import MainActions from '../actions/MainActions';
import localStorage from 'localStorage';
import store from '../store';


const initialState = {
  backgroundText: "BEST TUBE TO",
  selectedLanguage: null,
  comeOnDownVisible: true,
  directionsDisplayMode: 'overview',
  to: null,
  from: null,
  adverts: null,
  directions: null,
  stationmapref: null,
};

function MainReducer(state = initialState, action) {
  switch (action.type) {
    case MainActions.LOAD_SELECTED_LANGUAGE:
      var language = 'EN';
      if (localStorage.selectedLanguage !== undefined){
        language = localStorage.selectedLanguage;
      }
      return Object.assign({}, state, {
        selectedLanguage: language,
      });
    case MainActions.LOAD_SELECTED_STATIONS:
      var stationTo, stationFrom;

      if (localStorage.selectedStationTo !== undefined) {
        stationTo = localStorage.selectedStationTo;
      }
      if (localStorage.selectedStationFrom !== undefined) {
        stationFrom = localStorage.selectedStationFrom;
      }
      return Object.assign({}, state, {
        to: stationTo,
        from: stationFrom,
      });
    case MainActions.CHANGE_STATION:
      if (action.stationType === 'to') {
        return Object.assign({}, state, {
          to: action.newStationId,
        });
      }
      return Object.assign({}, state, {
        from: action.newStationId,
      });
    case MainActions.SET_LANGUAGE:
      localStorage.language = action.languageId;

      return Object.assign({}, state, {
        selectedLanguage: action.languageId,
      });
    case MainActions.FIND_DIRECTIONS_REQUEST:
      return Object.assign({}, state, {
        from: action.from,
        to: action.to,
      });
    case MainActions.FIND_DIRECTIONS_SUCCESS:

      localStorage.selectedStationTo = action.to;
      localStorage.selectedStationFrom = action.from;

      return Object.assign({}, state, {
        from: action.from,
        to: action.to,
        directions: action.directions,
        adverts: action.adverts,
        stationmapref: action.stationmapref,
      });

    case MainActions.FIND_DIRECTIONS_FAILED:
      return Object.assign({}, state, {
        error: action.error,
      });
    case MainActions.SET_COME_ON_DOWN_VISIBLE:
      return Object.assign({}, state, {
        comeOnDownVisible: action.visible,
      });
    case MainActions.SET_DIRECTIONS_DISPLAY_MODE:
      return Object.assign({}, state, {
        directionsDisplayMode: action.mode,
      });
    case MainActions.SWAP_DIRECTIONS:
      var tmpStation = state.from;
      var newStationFrom = state.to;
      var newStationTo = tmpStation;


      return Object.assign({}, state, {
        from: newStationFrom,
        to: newStationTo,
      });
    case MainActions.SET_BACKGROUND_TEXT:
      return Object.assign({}, state, {
        backgroundText: action.backgroundText,
      });

    default:
      return state;
  }
}

export default MainReducer;
