import MapActions from '../actions/MapActions';
import MAP_ICONS from '../data/map_icons';
const initialState = {
  markers: [],
  selectedMarker: null,
};

function MapReducer(state = initialState, action) {
  switch (action.type) {
    case MapActions.ADD_MARKER:
      var data = {
        id: action.id,
        data: action.data,
      }
      var newMarkers = state.markers.slice(0);
      newMarkers.push(data);
      return Object.assign({}, state, {
        markers: newMarkers,
      });
    case MapActions.REMOVE_MARKER:
      state.markers.splice(index, 1);
      return Object.assign({}, state, {

      });
    case MapActions.CLEAR_MARKERS:
      return Object.assign({}, state, {
        markers: [],
        selectedMarker: null,
      });
    case MapActions.MOVE_TO_MARKER:
      var newState = Object.assign({}, state, {
        selectedMarker: action.markerId,
      });
      var selectedMarkerObj = null;
      for(var i=0;i<state.markers.length;i+=1){
        var marker = state.markers[i];
        if (marker.data.id !== "stationid") {
          if (marker.data.id === action.markerId) {
            marker.data.icon = MAP_ICONS.hover.src + "|" + MAP_ICONS.hover.colour;
          }
          else {
            marker.data.icon = MAP_ICONS.default.src + "|" + MAP_ICONS.default.colour;
          }
        }
      }
      return newState;
    case MapActions.OPEN_MAP:
      return Object.assign({}, state, {
        mapOpen: true,
      });

    case MapActions.CLOSE_MAP:
      return Object.assign({}, state, {
        mapOpen: false,
      });
    case MapActions.MAP_LOADED:
      return Object.assign({}, state, {

      });
    default:
      return state;
  }
}

export default MapReducer;
